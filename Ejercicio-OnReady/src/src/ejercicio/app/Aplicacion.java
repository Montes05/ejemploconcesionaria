package src.ejercicio.app;

import src.ejercicio.clases.Lista;


public class Aplicacion {

  
    public static void main(String[] args) {
        
        Lista listaconcesionaria = new Lista();

        listaconcesionaria.llenarLista();
        listaconcesionaria.mostrarLista();
        listaconcesionaria.imprimirSeparador();
        listaconcesionaria.masCaro();
        listaconcesionaria.masBarato();
        listaconcesionaria.modeloconletraY();
        listaconcesionaria.imprimirSeparador();
        listaconcesionaria.ordenarxPrecio();

    }
    
}
