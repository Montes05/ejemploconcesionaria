package src.ejercicio.clases;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Lista {

    private List<Vehiculo> listaVehiculo;

    public Lista() {
        this.listaVehiculo = new ArrayList<Vehiculo>();
    }

    public void llenarLista() {

        Vehiculo peugeot206 = new Coche("Peugeot", "206", 4, 200000);
        Vehiculo hondaTitan = new Moto("Honda", "Titan", 125, 60000);
        Vehiculo peugeot208 = new Coche("Peugeot", "208", 5, 250000);
        Vehiculo yamahaybr = new Moto("Yamaha", "YBR", 160, 80500.5);

        listaVehiculo.add(peugeot206);
        listaVehiculo.add(hondaTitan);
        listaVehiculo.add(peugeot208);
        listaVehiculo.add(yamahaybr);
    }

    public void mostrarLista() {

        for (Vehiculo vehiculo : listaVehiculo) {
            System.out.println(vehiculo.ToString());
        }
    }

    public void imprimirSeparador() {
        System.out.println("==============================");
    }

    public void ordenarxPrecio() {
        Collections.sort(listaVehiculo);
        for (int i = 0; i < listaVehiculo.size(); i++) {
            System.out.println(listaVehiculo.get(i).getMarca()
                    + " " + listaVehiculo.get(i).getModelo());
        }
    }

    public void masCaro() {
        Collections.sort(listaVehiculo);
        System.out.println("Vehiculo más caro: "
                + listaVehiculo.get(0).getMarca() + " "
                + listaVehiculo.get(0).getModelo());
    }

    public void masBarato() {

        System.out.println("Vehiculo más barato: "
                + listaVehiculo.get(listaVehiculo.size() - 1).getMarca() + " "
                + listaVehiculo.get(listaVehiculo.size() - 1).getModelo());
    }

    public void modeloconletraY() {

        for (int i = 0; i < listaVehiculo.size(); i++) {

            String dondebuscar = listaVehiculo.get(i).getModelo();
            char letra = 'Y';
            for (int j = 0; j < dondebuscar.length() ; j++) {
                if (dondebuscar.charAt(j) == letra ) {
                    System.out.println("Vehiculo que contiene la letra 'Y': "+
                            listaVehiculo.get(i).getMarca()+" "+
                            listaVehiculo.get(i).getModelo()+" $"+
                            listaVehiculo.get(i).getPrecio());   
                }
            }
        }
    }
}
