package src.ejercicio.clases;

public class Moto extends Vehiculo {

    protected int cilindrada;

    public Moto(String marca, String modelo, int cilindrada, double precio) {
        super(marca, modelo, precio);
        this.cilindrada = cilindrada;

    }

    @Override
    public String ToString() {
        return "Marca: " + marca + "  //   Modelo: " + modelo
                + "   //  Cilindrada: " + cilindrada + "c" +
                "   //   Precio:  $" + precio;
    }

    public int getCilindrada() {
        return cilindrada;
    }

    public void setCilindrada(int cilindrada) {
        this.cilindrada = cilindrada;
    }

}
