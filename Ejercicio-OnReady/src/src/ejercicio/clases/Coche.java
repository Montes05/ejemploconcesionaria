package src.ejercicio.clases;

public class Coche extends Vehiculo {

    protected int puerta;

    public Coche(String marca, String modelo, int puerta, double precio) {
        super(marca, modelo, precio);
        this.puerta = puerta;
    }

    @Override
    public String ToString() {
        return "Marca: " + marca + "  //   Modelo: " + modelo
                + "   //  Puertas: " + puerta + "   //   Precio:  $" + precio;
    }

    public int getPuerta() {
        return puerta;
    }

    public void setPuerta(int puerta) {
        this.puerta = puerta;
    }

}
