package src.ejercicio.clases;

public class Vehiculo implements Comparable<Vehiculo> {

    protected String marca;
    protected String modelo;
    protected double precio;

    public Vehiculo(String marca, String modelo, double precio) {
        this.marca = marca;
        this.modelo = modelo;
        this.precio = precio;

    }

    public String ToString() {
        return "Marca: " + marca + "///   Modelo: " + modelo
                + "   ///   Precio:  $" + precio;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public int compareTo(Vehiculo v) {
        return (int) (v.precio - this.precio);
    }

}
